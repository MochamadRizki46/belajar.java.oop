package belajar.java.oop.kelasabstrakmethod;

public class AnimalApp {

    public static void main(String[] args) {

        Animal animal = new Cat();
        animal.name = "Putih";
        animal.run();
    }
}
