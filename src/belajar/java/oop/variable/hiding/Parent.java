package belajar.java.oop.variable.hiding;

class Parent {

    String name;
    void doIt(){
        System.out.println("Do it in Parent Class");
    }
}

class Child extends Parent{

    String name;
    void doIt(){
        System.out.println("Do it in Child Class");
    }
}
