package belajar.java.oop.kelassuper;

public class ShapeApp {

    public static void main(String[] args) {

        var sh = new Shape();
        System.out.println(sh.getCorner());

        var rc = new Rectangle();
        System.out.println(rc.getCorner());
        System.out.println(rc.getParentCorner());

    }
}
