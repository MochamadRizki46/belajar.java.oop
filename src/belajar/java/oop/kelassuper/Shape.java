package belajar.java.oop.kelassuper;

//super. untuk akses method telah ter OVERRIDE
class Shape {

    int getCorner(){
        return 0;
    }
}

class Rectangle extends Shape{

    int getCorner(){
        return 4;
    }

    int getParentCorner(){
        return super.getCorner();
    }
}
