package belajar.java.oop.interfacess;

public class CarApp {

    public static void main(String[] args) {

        Car car = new Avanza();
        car.drive();
        System.out.println("Tire  = "+car.getTire());
        System.out.println("Brand = "+car.getBrand());

        Car car2 = new Bus();
        car2.drive();
        System.out.println("Tire  = "+car2.getTire());
        System.out.println("Brand = "+car2.getBrand());
    }
}
