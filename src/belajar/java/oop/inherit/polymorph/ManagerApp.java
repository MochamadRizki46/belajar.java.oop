package belajar.java.oop.inherit.polymorph;

public class ManagerApp {

    public static void main(String[] args) {

        var man = new Manager("test Manager");
        man.sayHello("Budi");

        var vp = new VicePresident("test VicePresident");
        vp.sayHello("Mochamad");

//        var man = new Manager();
//        man.name = "Rizki";
//        man.sayHello("Budi");
//
//        var vp = new VicePresident();
//        vp.name = "Adityo";
//        vp.sayHello("Budi");

    }
}
