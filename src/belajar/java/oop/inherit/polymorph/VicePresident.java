package belajar.java.oop.inherit.polymorph;

class VicePresident extends Manager {

    VicePresident(String name){
        super(name);
    }

//    VicePresident(String name, String company){
//        this.name = name;
//        this.company = company;
//    }
//
//    VicePresident(){
//
//    }
//
    void sayHello(String name){
        System.out.println("Hallo "+ name + ", Nama VicePresident anda "+ this.name);

    }
}
