package belajar.java.oop.inherit.polymorph;

class Manager extends Employee {

    public String company;

    Manager(String name){
        super(name);
    }

    Manager(String name, String company){
        super(name);
        this.company = company;
    }

//    Manager(){
//
//    }

    void sayHello(String name){
        System.out.println("Hallo "+ name + ", Nama Manager anda "+ this.name);

    }
}
