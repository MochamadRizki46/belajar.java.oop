package belajar.java.oop.inherit.polymorph;

public class PolymorphismApp {

    public static void main(String[] args) {

        //Polymorphism itu bisa berubah bentuk
        //Jadi klo deklarasinya Employee... KYK gini
        //Tipe Variable nya OBJEK EMPLOYEE tapi bisa diakses dari MANAGER atau VICEPRESIDENT... kyk gini:
        Employee employee = new Employee("Eko");
        employee.sayHello("Mochamad");

        employee = new Manager("Eko");
        employee.sayHello("Rizki");

        employee = new VicePresident("Eko");
        employee.sayHello("Adityo");

        //Cara panggil polymorphism method
        sayHello(new Employee("Employee R1"));
        sayHello(new Manager("Manager R1"));
        sayHello(new VicePresident("Vice President R1"));
    }

    //polymorphisme method
    static void sayHello(Employee employee){
        //pengecekan cast/saat konversi
        if (employee instanceof VicePresident){
            VicePresident vicePresident = (VicePresident) employee;
            System.out.println("Hello Vice President"+ vicePresident.name);
        } else if (employee instanceof Manager){
            Manager manager = (Manager) employee;
            System.out.println("Hello Manager"+ manager.name);
        } else {
            System.out.println("Hello "+ employee.name);
        }

    }
}
