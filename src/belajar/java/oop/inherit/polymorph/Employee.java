package belajar.java.oop.inherit.polymorph;

class Employee {

    String name;

    Employee (String name){
        this.name = name;
    }

    void sayHello(String name){
        System.out.println("Hallo "+ name + " Nama Employee "+ this.name);

    }
}
